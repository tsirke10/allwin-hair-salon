﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace AllwinHairdresserSalon.Models
{
    public enum HaircutType
    {
        [Display(Name = "Female (60 minutes)")]
        Female,
        [Display(Name = "Male (30 minutes)")]
        Male
    }
}
