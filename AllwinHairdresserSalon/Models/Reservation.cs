﻿using System;
using System.ComponentModel.DataAnnotations;

namespace AllwinHairdresserSalon.Models
{
    public class Reservation
    {
        public int Id { get; set; }
        public string UserId { get; set; }

        [Display(Name = "Time")]
        [DataType(DataType.DateTime)]
        public DateTime StartTime { get; set; }

        public int Duration { get; set; }

        [Display(Name = "Haircut type")]
        public HaircutType HaircutType { get; set; }
    }
}
